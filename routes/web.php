<?php

use BaconQrCode\Encoder\QrCode;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SuperAdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\QrCodeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/login', function () {
//     return view('auth/login');
// });

Route::get('/', function () {
    return view('landingpage');
});

// Route::middleware('guest')->group(function);

Route::name('superadmin.')->middleware('superadmin')->group(function () {
    // Route::get('/superadmin', [HomeController::class, 'superadminhome'])->name('home');
    // Route::get('/list-admin', [AdminController::class, 'index'])->name('list-admin');
    // Route::get('/create-admin', [AdminController::class, 'create'])->name('create-admin');
    // Route::post('/store-admin', [AdminController::class, 'store'])->name('store-admin');
    Route::get('/superadmin', function () {
        return view('superadmin.superadminHome');
    })->name('home');

    Route::get('/addAdmin', [SuperAdminController::class, 'index'])->name('addAdmin');
    Route::get('/deleteAdmin/{id}', [SuperAdminController::class, 'delete'])->name('deleteAdmin');
    Route::get('/listUser', [SuperAdminController::class, 'indexUser'])->name('listUser');
});

Route::name('admin.')->middleware('admin')->group(function () {
    //admin home
    Route::get('/admin', [AdminController::class, 'indexAdmin'])->name('home');
    Route::get('/adminTransaction', [AdminController::class, 'transaction'])->name('admin-transaction');
    Route::get('/approved/{id_admin}/{id_order}', [AdminController::class, 'approved'])->name('approved');

    Route::get('/listUser', [SuperAdminController::class, 'indexUser'])->name('listUser');

    // Notifikasi
    Route::get('/read-notifikasi', [NotificationController::class, 'read'])->name('read-notifikasi');
    Route::get('/create-notifikasi', [NotificationController::class, 'create'])->name('create-notifikasi');;
    Route::post('/store-notifikasi', [NotificationController::class, 'store'])->name('store-notifikasi');
    Route::get('/jumlah-notifikasi', [NotificationController::class, 'jumlahNotif'])->name('jumlah-notifikasi');
    Route::get('/admin-getnotifpop', [NotificationController::class, 'getNotifPop'])->name('admin-getNotifPop');
    Route::get('/bacaNotif/{id}', [NotificationController::class, 'bacaNotif'])->name('bacaNotif');


    // QrCode
    Route::get('/create-qrcode', [QrCodeController::class, 'create'])->name('create-qrcode');
    Route::post('/store-qrcode', [QrCodeController::class, 'store'])->name('store-qrcode');


    // Route::get('/adminTransaction', function () {
    //     return view('admin.adminTransaction');
    // })->name('addAdmin');
});

Route::name('user.')->middleware('user')->group(function () {
    // user home
    Route::get('/user', [UserController::class, 'notification'])->name('home');
    Route::get('/userCart', [UserController::class, 'cart'])->name('userCart');
    Route::get('/userCheckout', [UserController::class, 'checkout'])->name('userCheckout');
    Route::post('/userCheckout/{id}', [UserController::class, 'orderNow'])->name('orderNow');
    // Route::get('/userCheckout', [UserController::class, 'checkout'])->name('userCheckout');


    // Notifikasi
    // Route::get('/userHome', [App\Http\Controllers\NotificationController::class, 'index'])->name('userHome');
    Route::post('/userHome', [NotificationController::class, 'store']);
    Route::get('/read-notifikasi', [NotificationController::class, 'read'])->name('read-notifikasi');
    Route::get('/jumlah-notifikasi', [NotificationController::class, 'jumlahNotif'])->name('jumlah-notifikasi');
    Route::get('/bacaNotif/{id}', [NotificationController::class, 'bacaNotif'])->name('bacaNotif');
    Route::get('/user-getnotifpop', [NotificationController::class, 'getNotifPop'])->name('user-getNotifPop');

    // Route::get('/notifikasi', [NotificationController::class, 'index'])->name('notifikasi');
    // Route::get('/user/profile', function () {
    //     // Uses first & second middleware...
    // });
});


Auth::routes(['verify' => true]);

// Home admin
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');




// Chart
Route::get('/chart', [App\Http\Controllers\ChartController::class, 'index']);
