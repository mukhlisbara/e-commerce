<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')

    @yield('style')
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('layouts.sidebar')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('layouts.header')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                @yield('content')
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @include('layouts.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="/home">Logout</a>
                </div>
            </div>
        </div>
    </div>
</body>
<!-- Bootstrap core JavaScript-->
<script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Core plugin JavaScript-->
<script src="{{ asset('/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

<!-- Custom scripts for all pages-->
<script src="{{ asset('/js/sb-admin-2.min.js') }}"></script>

@if (Auth::user()->id_role === 3)
    {{-- pwa --}}
    <meta name="theme-color" content="#6777ef" />
    <link rel="apple-touch-icon" href="{{ asset('logo.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">

    {{-- pwa --}}
    <script src="{{ asset('/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("/sw.js").then(function(reg) {
                console.log("Service worker has been registered for scope: " + reg.scope);
            });
        }
    </script>
@endif
<!-- PWA  -->


{{-- Notifikasi --}}
{{-- <script>
        function showNotification() {
            const notification = new Notification("Pesan baru!", {
                body: "Hey Bara, gimana kabarnya?",
                icon: "{{ asset('logo.png') }}"
            });
            notification.onclick = (e) => {
                window.location.href = "https://google.com";
            };
        }

        console.log(Notification.permisson);

        if (Notification.permisson === "granted") {
            alert("kita butuh perhatian");
        } else if (Notification.permisson !== "denied") {
            Notification.requestPermission().then(permission => {
                if (permission === "granted") {
                    showNotification();
                }
            });+
        }
    </script> --}}

<!-- Core Javascripts for Datatable -->
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script>
    $(document).ready(function() {
        setInterval(function() {
            read();
            jumlahNotif();
            AjaxFunction();
        }, 5000);
        // setInterval(jumlahNotif, 1000);
        // setInterval(AjaxFunction, 1000);
    });
</script>

{{-- fungsi read notif di database --}}

<script>
    function read() {
        $.get("{{ url('read-notifikasi') }}", {}, function(data_notifications, status) {
            $("#read").html('');
            $("#baca").html(data_notifications['getNotif']);
            const link = "{{ Auth::user()->id_role == 2 ? route('admin.admin-transaction') : '' }}";
            for (let index = 0; index < data_notifications['getNotif'].length; index++) {
                const notif = data_notifications['getNotif'][index];
                $("#read").append(
                    '<a class="dropdown-item d-flex align-items-center" href="' + link +
                    '" onclick="dibaca(' +
                    notif[
                        "id"] + ')">' + '<div class="mr-3">' + '<div class="icon-circle bg-primary">' +
                    '<i class="fas fa-file-alt text-white"></i>' +
                    '</div>' +
                    '</div>' +
                    '<div>' +
                    '<div class="font-weight-bold"> ' + notif["judul"] + '</div>' +
                    '<span class="small text-gray-500"> ' + notif["isi_pesan"] + '</span>' +
                    '</div>' +
                    '</a>'
                );
            }
            // console.log(data_notifications);
        });
    }

    function dibaca(id) {
        $.get("{{ url('bacaNotif') }}/" + id, {}, function() {});
        var selector = $("#clickNotif").html();
        if (selector - 1 > 0) {
            $("#clickNotif").html(selector - 1);
        } else {
            $("#clickNotif").remove();
        }
    }

    function jumlahNotif() {

        $.get("{{ url('jumlah-notifikasi') }}", {}, function(jumlah_notifikasi, status) {
            let bell = $(".jmlh_notif");
            // if (document.getElementsByClassName("jmlh_notif")) {
            //     document.getElementsByClassName("jmlh_notif").remove();
            // }
            if ($('#clickNotif')) {
                $("#clickNotif").remove();
            }
            if (jumlah_notifikasi !== 0) {
                $('<span id="clickNotif" class="badge badge-danger badge-counter">' + jumlah_notifikasi +
                        '</span>')
                    .appendTo(
                        ".bara");
            }
        });
    }
</script>

{{-- pop up notifikasi --}}
{{--  --}}
@yield('script')



</html>
