@if (Auth::user()->id_role === 1)
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
            </div>
            <div class="sidebar-brand-text mx-3">Toko Kelontong</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li
            class="nav-item {{ Route::is('superadmin.home') || Route::is('admin.home') || Route::is('user.home') ? 'active' : '' }}">
            <a class="nav-link"
                href="{{ Auth::user()->id_role === 1 ? '/superadmin' : (Auth::user()->id_role === 2 ? '/admin' : '/user') }}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>


        {{-- <!-- Heading -->
    <div class="sidebar-heading">
        Interface
    </div> --}}

        <!-- Nav Item - Pages Collapse Menu -->
        @if (Auth::user()->id_role === 1)
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Admin</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Access Admin:</h6>
                        <a class="collapse-item" href="/addAdmin">List Admin</a>
                        <a class="collapse-item" href="/editAdmin">Edit Admin</a>
                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>User</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Access User:</h6>
                        <a class="collapse-item" href="/listUser">List User</a>
                        <a class="collapse-item" href="/blockUser">Block User</a>
                    </div>
                </div>
            </li>
        @endif

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
@endif

@if (Auth::user()->id_role === 2)
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
            </div>
            <div class="sidebar-brand-text mx-3">Toko Kelontong</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li
            class="nav-item {{ Route::is('superadmin.home') || Route::is('admin.home') || Route::is('user.home') ? 'active' : '' }}">
            <a class="nav-link"
                href="{{ Auth::user()->id_role === 1 ? '/superadmin' : (Auth::user()->id_role === 2 ? '/admin' : '/user') }}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
        <!-- Nav Item - Pages Collapse Menu -->


        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Admin only access</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Access User:</h6>
                    <a class="collapse-item" href="/listUser">List User</a>
                    <a class="collapse-item" href="/editAdmin">Block User</a>
                </div>
            </div>
        </li>

        <li class="nav-item {{ Route::is('admin.create-qrcode') ? 'active' : '' }}">
            <a class="nav-link" href="/create-qrcode">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>QrCode</span></a>
        </li>

        <li class="nav-item {{ Route::is('admin.adminTransaction') ? 'active' : '' }}">
            <a class="nav-link" href="/adminTransaction">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>Transaction</span></a>
        </li>

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
@endif
