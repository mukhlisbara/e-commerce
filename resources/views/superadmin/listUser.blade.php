@extends('layouts.app')

@section('style')
    <!-- Custom styles for this page -->
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.min.css'>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-2 text-gray-800">List User</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm ok"><i
                    class="fa-solid fa-plus mr-2" aria-hidden="true"></i>Add User</a>
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary align-right">List User</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nama User</th>
                                {{-- <th>Nama Toko</th> --}}
                                <th>Email</th>
                                <th>Jenis Kelamin</th>
                                <th>Alamat</th>
                                <th>No. Telp</th>
                                <th>Dibuat Tanggal</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data_users as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    {{-- <td>{{ $item->store_name }}</td> --}}
                                    <td>{{ $item->email }}</td>
                                    <td>
                                        @if ($item->gender == 1)
                                            {{ 'Laki laki' }}
                                        @else
                                            {{ 'Perempuan' }}
                                        @endif
                                    </td>
                                    <td>{{ $item->address }}</td>
                                    <td>{{ $item->phone }}</td>
                                    <td>{{ $item->created_at->format('d F Y') }}</td>
                                    <td>{{ $item->status }}</td>
                                    <td><a href="#" class="btn btn-primary btn-sm active" role="button"
                                            aria-pressed="true"><i class="fa-solid fa-pen-to-square"></i></a>
                                        <a href="{{ route('superadmin.deleteAdmin', ['id' => $item->id]) }}"
                                            class="btn btn-danger btn-sm active ok " role="button" aria-pressed="true"><i
                                                class="fa-solid fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
    <!-- Page level custom scripts -->
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();
        });
    </script>

    {{-- sweet alert --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.26/dist/sweetalert2.all.min.js"></script>
    <script>
        var ok = document.getElementsByClassName("ok");
        for (var i = 0; i < ok.length; i++) {
            ok[i].addEventListener('click', function(e) {
                e.preventDefault();
                var tujuan = this.href;
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Delete Now!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = (tujuan);
                        Swal.fire({
                            title: 'Action Success!',
                            html: 'Your action has been success.',
                            icon: 'success',
                            showConfirmButton: false,
                        });
                    }
                })

            });
        }
    </script>
@endsection
