@extends('layouts.app')

<head>
    <meta charset="utf-8">
    <title>How to Generate QR Code in Laravel 8</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" />
</head>

@section('content')
    <div class="container mt-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Create QrCode</h6>
            </div>
            <div class="card-body">
                <div class="row row-cols-3">
                    <div class="col">
                        <form action="/store-qrcode" method="post">
                            @csrf
                            <label for="qrcode">Masukkan isi QrCode :</label>
                            <input id="qrcode"type="text" name="qrcode" required>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>

                    <div class="col">
                        <h5>Simple QR Code</h5>
                        <div class="card-body">
                            {{ QrCode::size(300)->generate($qrcode) }}
                        </div>
                        <a href="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(300)->generate($qrcode)) }}"
                            download="qrcode"><button type="download" class="btn btn-primary">Download</button></a>
                    </div>

                    <div class="col">
                        <h5>Color QR Code</h5>
                        <div class="card-body">
                            {{ QrCode::size(300)->backgroundColor(255, 90, 0)->generate($qrcode) }}
                        </div>
                        <a href="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(300)->generate($qrcode)) }}"
                            download="qrcode"><button type="download" class="btn btn-primary">Download</button></a>
                    </div>
                </div>

            </div>
        </div>

        {{-- <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button"
                aria-expanded="false" aria-controls="collapseCardExample">
                <h6 class="m-0 font-weight-bold text-primary">History QrCode</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse" id="collapseCardExample" style="">
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">QrCode</th>
                                <th scope="col">Isi Data</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($qrcodes as $item)
                                <tr>

                                    <td>{{ QrCode::size(200)->generate($item->qrcode) }}</td>
                                    <td>{{ $item->qrcode }}</td>
                                    <td><a href="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(300)->generate($qrcode)) }}"
                                            download="qrcode"><button type="download"
                                                class="btn btn-primary">Download</button></a></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div> --}}

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary align-right">History QrCode</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0"
                                    role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                    <thead>
                                        <tr role="row">
                                            <th>QrCode</th>
                                            <th>Isi Data</th>
                                            <th>>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (@isset($qrcodes))
                                            @foreach ($qrcodes as $item)
                                                <tr>

                                                    <td>{{ QrCode::size(200)->generate($item->qrcode) }}</td>
                                                    <td>{{ $item->qrcode }}</td>
                                                    <td><a href="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(300)->generate($qrcode)) }}"
                                                            download="qrcode"><button type="download"
                                                                class="btn btn-primary">Download</button></a></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();
        });
    </script>
@endsection
