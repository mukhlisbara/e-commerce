@extends('layouts.app')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
        </div>

        <!-- Content Row -->
        <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Jumlah Admin (Store)</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">15</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-calendar fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    Jumlah User (Buyer)</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">200</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tasks
                                </div>
                                <div class="row no-gutters align-items-center">
                                    <div class="col-auto">
                                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
                                    </div>
                                    <div class="col">
                                        <div class="progress progress-sm mr-2">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%"
                                                aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                    Pending Requests</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">18</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Content Row -->
        <div class="row">
            <!-- Area Chart -->
            <div class="col-lg-6 mb-4">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 ">
                        <h6 class="m-0 font-weight-bold text-primary">Data Lokasi User</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body mb-5">
                        <div class="col">
                            {{-- chart column --}}
                            <div id="chart_bar"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Jumlah User</h6>
                    </div>

                    <!-- Card Body -->
                    <div class="card-body">
                        {{-- chart pie --}}
                        <div class="col">
                            <div id="chart_simple"></div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Content column chart --}}
            <div class="col-lg-6 mb-4">
                <!-- Project Card Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Data Lokasi User</h6>
                    </div>
                    <div class="card-body">
                        {{-- chart column --}}
                        <div class="col">
                            <div id="chart_column"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Content polar -->
            <div class="col-lg-6 mb-4">
                <!-- Project Card Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Jumlah User</h6>
                    </div>
                    <div class="card-body">
                        {{-- chart polar --}}
                        <div class="col">
                            <div id="chart_polar"></div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Pesan notifikasi --}}
            <div class="col-lg-6 mb-4">
                <!-- Approach -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Pesan Notifikasi</h6>
                    </div>
                    <div class="card-body">
                        <form action="/store-notifikasi" method="post">
                            @csrf
                            <label for="judul">Judul Notifikasi : </label>
                            <input id="judul" type="text" name="judul" required><br>
                            <label for="isi">Isi Notifikasi : </label>
                            <input id="isi" type="text" name="isi_pesan" required>
                            <button type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <!-- Page level plugins -->
    <script src="{{ asset('/vendor/chart.js/Chart.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('/js/demo/chart-area-demo.js') }}"></script>
    <script src="{{ asset('/js/demo/chart-pie-demo.js') }}"></script>

    {{-- Script Pie Chart --}}
    <script>
        var Laki_laki = parseInt('{{ $Laki_laki }}');
        var Perempuan = parseInt('{{ $Perempuan }}');

        var options = {
            series: [Laki_laki, Perempuan],
            chart: {
                width: 380,
                type: 'pie',
            },
            labels: ['Laki laki', 'Perempuan'],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        };
        var chart_simple = new ApexCharts(document.querySelector("#chart_simple"), options);
        chart_simple.render();
    </script>

    {{-- Script Bar Chart --}}
    <script>
        var surabaya = {{ $surabaya }};
        var malang = {{ $malang }};

        var options = {
            series: [{
                data: [surabaya, malang]
            }],
            chart: {
                type: 'bar',
                height: 350
            },
            plotOptions: {
                bar: {
                    borderRadius: 4,
                    horizontal: true,
                }
            },
            dataLabels: {
                enabled: false
            },
            xaxis: {
                categories: ['Surabaya', 'Malang'],
            },
        };

        var chart_bar = new ApexCharts(document.querySelector("#chart_bar"), options);
        chart_bar.render();
    </script>

    {{-- script column chart --}}
    <script>
        var surabaya = {{ $surabaya }};
        var malang = {{ $malang }};

        var options = {
            series: [surabaya, malang],
            chart: {
                width: 480,
                type: 'bar',
            },
            series: [{
                name: 'Surabaya',
                data: [surabaya]
            }, {
                name: 'Malang',
                data: [malang]
            }],

            chart: {
                type: 'bar',
                height: 350
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: ['User'],
            },
            yaxis: {
                title: {
                    text: 'Jumlah user'
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function(val) {
                        return val + " user"
                    }
                }
            }
        };

        var chart = new ApexCharts(document.querySelector("#chart_column"), options);
        chart.render();
    </script>

    {{-- script polar area --}}
    <script>
        var Laki_laki = parseInt('{{ $Laki_laki }}');
        var Perempuan = parseInt('{{ $Perempuan }}');
        console.log(Laki_laki, Perempuan);

        var options = {
            series: [Laki_laki, Perempuan],
            chart: {
                width: 480,
                type: 'polarArea',
            },
            stroke: {
                colors: ['#fff']
            },
            fill: {
                opacity: 0.8
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        };

        var chart = new ApexCharts(document.querySelector("#chart_polar"), options);
        chart.render();
    </script>

    {{-- ### Script Nampilin Notifikasi ### --}}
    <script>
        function tampilnotifikasi(data) {
            if (Notification.premission === "granted") {
                navigator.serviceWorker.ready.then((registration) => {
                    registration.showNotification(data['judul'], {
                        body: data['isi_pesan'],
                        icon: "/logo.png",
                    });
                });
            } else if (Notification.permission !== "denied") {
                Notification.requestPermission(function(result) {
                    if (result === 'granted') {
                        navigator.serviceWorker.ready.then((registration) => {
                            registration.showNotification(data['judul'], {
                                body: data['isi_pesan'],
                                icon: "/logo.png",
                            });
                        });
                    }
                });
            }
        }

        function AjaxFunction() {
            var httpxml = new XMLHttpRequest();

            function stateck() {
                if (httpxml.readyState == 4) {
                    var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                    if (response) {
                        tampilnotifikasi(response);
                    } else {
                        return false;
                    }
                }
            }
            httpxml.onreadystatechange = stateck;
            httpxml.open("GET", "/admin-getnotifpop", true);
            httpxml.send();
        }
    </script>

    {{-- pwa --}}
    {{-- <script src="{{ asset('/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("/sw.js").then(function(reg) {
                console.log("Service worker has been registered for scope: " + reg.scope);
            });
        }
    </script> --}}
@endsection
