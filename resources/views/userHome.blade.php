@extends('layouts.app')

@section('style')
@endsection

@section('content')
    <!-- start page content -->
    <div class="container">
        <div class="row">
            <!-- start products section -->
            <div class="col-md-8 offset-md-1">
                <div class="head row">
                    <div class="col-md-6">
                        <h2 class="content-head">
                            {{-- {{ $categoryName }} --}}
                            Product
                        </h2>
                    </div>
                    <div class="col-md-6 text-right">
                        <span class='font-weight-bolder' style="font-size: 1.2em">Price: </span>
                        <span class="align-right"><a href="#" class="text-decoration-none">Low to
                                High</a></span>
                        <span class="align-right"><a href="#" class="text-decoration-none">High
                                to Low</a></span>
                    </div>
                </div>
                <!-- start products row -->
                <div class="row">
                    {{-- @foreach ($products as $product)
                        <!-- start single product -->
                        <div class="col-md-6 col-sm-12 col-lg-4 product">
                            <a href="{{ route('shop.show', $product->slug) }}" class="custom-card">
                                <div class="card view overlay zoom">
                                    <img src="{{ productImage($product->image) }}" class="card-img-top img-fluid"
                                        alt="..." height="200px" width="200px">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $product->name }}<span class="float-right">$
                                                {{ format($product->price) }}</span></h5>
                                        {{-- <div class="product-actions" style="display: flex; align-items: center; justify-content: center">
                                            <a class="cart" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fas fa-cart-plus"></i></a>
                                            <a class="like" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fa fa-thumbs-up"></i></a>
                                            <a class="heart" href="#"><i style="color:blue; font-size: 1.3em" class="fa fa-heart-o"></i></a>
                                        </div> --}}
                    {{-- </div>
                                </div>
                            </a>
                        </div> --}}
                    <!-- end single product -->
                    {{-- @endforeach --}}

                    <!-- start single product -->
                    <div class="col-md-6 col-sm-12 col-lg-4 mb-4 product">
                        <a href="#" class="custom-card">
                            <div class="card view overlay zoom">
                                <img src="{{ asset('/img/portfolio/7.jpg') }}" class="card-img-top img-fluid" alt="..."
                                    height="200px" width="200px">
                                <div class="card-body">
                                    <h5 class="card-title">Ventela High Public Green Army<span class="float-right"></span>
                                    </h5>
                                    <a href="/userCart" class="btn btn-primary btn-sm active" role="button"
                                        aria-pressed="true"><i class="fa-solid fa-cart-shopping"></i></a>
                                    {{-- <div class="product-actions" style="display: flex; align-items: center; justify-content: center">
                                            <a class="cart" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fas fa-cart-plus"></i></a>
                                            <a class="like" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fa fa-thumbs-up"></i></a>
                                            <a class="heart" href="#"><i style="color:blue; font-size: 1.3em" class="fa fa-heart-o"></i></a>
                                        </div> --}}
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-12 col-lg-4 mb-4 product">
                        <a href="#" class="custom-card">
                            <div class="card view overlay zoom">
                                <img src="{{ asset('/img/portfolio/8.jpg') }}" class="card-img-top img-fluid" alt="..."
                                    height="200px" width="200px">
                                <div class="card-body">
                                    <h5 class="card-title">Ventela High Public Cream-White<span class="float-right"></span>
                                    </h5>
                                    <a href="/userCart" class="btn btn-primary btn-sm active  " role="button"
                                        aria-pressed="true"><i class="fa-solid fa-cart-shopping"></i></a>
                                    {{-- <div class="product-actions" style="display: flex; align-items: center; justify-content: center">
                                            <a class="cart" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fas fa-cart-plus"></i></a>
                                            <a class="like" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fa fa-thumbs-up"></i></a>
                                            <a class="heart" href="#"><i style="color:blue; font-size: 1.3em" class="fa fa-heart-o"></i></a>
                                        </div> --}}
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-12 col-lg-4 mb-4 product">
                        <a href="#" class="custom-card">
                            <div class="card view overlay zoom">
                                <img src="{{ asset('/img/portfolio/9.jpg') }}" class="card-img-top img-fluid" alt="..."
                                    height="200px" width="200px">
                                <div class="card-body">
                                    <h5 class="card-title">Ventela High Public Cream-Gum<span class="float-right"></span>
                                    </h5>
                                    <a href="/userCart" class="btn btn-primary btn-sm active  " role="button"
                                        aria-pressed="true"><i class="fa-solid fa-cart-shopping"></i></a>
                                    {{-- <div class="product-actions" style="display: flex; align-items: center; justify-content: center">
                                            <a class="cart" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fas fa-cart-plus"></i></a>
                                            <a class="like" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fa fa-thumbs-up"></i></a>
                                            <a class="heart" href="#"><i style="color:blue; font-size: 1.3em" class="fa fa-heart-o"></i></a>
                                        </div> --}}
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-12 col-lg-4 mb-4 product">
                        <a href="#" class="custom-card">
                            <div class="card view overlay zoom">
                                <img src="{{ asset('/img/portfolio/10.jpg') }}" class="card-img-top img-fluid"
                                    alt="..." height="200px" width="200px">
                                <div class="card-body">
                                    <h5 class="card-title">Ventela High Public While-Solid<span class="float-right"></span>
                                    </h5>
                                    <a href="/userCart" class="btn btn-primary btn-sm active  " role="button"
                                        aria-pressed="true"><i class="fa-solid fa-cart-shopping"></i></a>
                                    {{-- <div class="product-actions" style="display: flex; align-items: center; justify-content: center">
                                            <a class="cart" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fas fa-cart-plus"></i></a>
                                            <a class="like" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fa fa-thumbs-up"></i></a>
                                            <a class="heart" href="#"><i style="color:blue; font-size: 1.3em" class="fa fa-heart-o"></i></a>
                                        </div> --}}
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-12 col-lg-4 mb-4 product">
                        <a href="#" class="custom-card">
                            <div class="card view overlay zoom">
                                <img src="{{ asset('/img/portfolio/11.jpg') }}" class="card-img-top img-fluid"
                                    alt="..." height="200px" width="200px">
                                <div class="card-body">
                                    <h5 class="card-title">Ventela High Public Black<span class="float-right"></span></h5>
                                    <a href="/userCart" class="btn btn-primary btn-sm active  " role="button"
                                        aria-pressed="true"><i class="fa-solid fa-cart-shopping"></i></a>
                                    {{-- <div class="product-actions" style="display: flex; align-items: center; justify-content: center">
                                            <a class="cart" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fas fa-cart-plus"></i></a>
                                            <a class="like" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fa fa-thumbs-up"></i></a>
                                            <a class="heart" href="#"><i style="color:blue; font-size: 1.3em" class="fa fa-heart-o"></i></a>
                                        </div> --}}
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-12 col-lg-4 mb-4 product">
                        <a href="#" class="custom-card">
                            <div class="card view overlay zoom">
                                <img src="{{ asset('/img/portfolio/12.jpg') }}" class="card-img-top img-fluid"
                                    alt="..." height="200px" width="200px">
                                <div class="card-body">
                                    <h5 class="card-title">Ventela Low-Cut Public Dark Green <span
                                            class="float-right"></span></h5>
                                    <a href="/userCart" class="btn btn-primary btn-sm active" role="button"
                                        aria-pressed="true"><i class="fa-solid fa-cart-shopping"></i></a>
                                    {{-- <div class="product-actions" style="display: flex; align-items: center; justify-content: center">
                                            <a class="cart" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fas fa-cart-plus"></i></a>
                                            <a class="like" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fa fa-thumbs-up"></i></a>
                                            <a class="heart" href="#"><i style="color:blue; font-size: 1.3em" class="fa fa-heart-o"></i></a>
                                        </div> --}}
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-12 col-lg-4 mb-4 product">
                        <a href="#" class="custom-card">
                            <div class="card view overlay zoom">
                                <img src="{{ asset('/img/portfolio/13.jpg') }}" class="card-img-top img-fluid"
                                    alt="..." height="200px" width="200px">
                                <div class="card-body">
                                    <h5 class="card-title">Ventela Low-Cut Public Black <span class="float-right"></span>
                                    </h5>
                                    <a href="/userCart" class="btn btn-primary btn-sm active  " role="button"
                                        aria-pressed="true"><i class="fa-solid fa-cart-shopping"></i></a>
                                    {{-- <div class="product-actions" style="display: flex; align-items: center; justify-content: center">
                                            <a class="cart" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fas fa-cart-plus"></i></a>
                                            <a class="like" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fa fa-thumbs-up"></i></a>
                                            <a class="heart" href="#"><i style="color:blue; font-size: 1.3em" class="fa fa-heart-o"></i></a>
                                        </div> --}}
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-12 col-lg-4 mb-4 product">
                        <a href="#" class="custom-card">
                            <div class="card view overlay zoom">
                                <img src="{{ asset('/img/portfolio/14.jpg') }}" class="card-img-top img-fluid"
                                    alt="..." height="200px" width="200px">
                                <div class="card-body">
                                    <h5 class="card-title">Ventela Low-Cut Public Oreo<span class="float-right"></span>
                                    </h5>
                                    <a href="/userCart" class="btn btn-primary btn-sm active  " role="button"
                                        aria-pressed="true"><i class="fa-solid fa-cart-shopping"></i></a>
                                    {{-- <div class="product-actions" style="display: flex; align-items: center; justify-content: center">
                                            <a class="cart" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fas fa-cart-plus"></i></a>
                                            <a class="like" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fa fa-thumbs-up"></i></a>
                                            <a class="heart" href="#"><i style="color:blue; font-size: 1.3em" class="fa fa-heart-o"></i></a>
                                        </div> --}}
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-12 col-lg-4 mb-4 product">
                        <a href="#" class="custom-card">
                            <div class="card view overlay zoom">
                                <img src="{{ asset('/img/portfolio/15.jpg') }}" class="card-img-top img-fluid"
                                    alt="..." height="200px" width="200px">
                                <div class="card-body">
                                    <h5 class="card-title">Ventela Low-Cut Public White-Solid<span
                                            class="float-right"></span></h5>
                                    <a href="/userCart" class="btn btn-primary btn-sm active  " role="button"
                                        aria-pressed="true"><i class="fa-solid fa-cart-shopping"></i></a>
                                    {{-- <div class="product-actions" style="display: flex; align-items: center; justify-content: center">
                                            <a class="cart" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fas fa-cart-plus"></i></a>
                                            <a class="like" href="#" style="margin-right: 1em"><i style="color:blue; font-size: 1.3em" class="fa fa-thumbs-up"></i></a>
                                            <a class="heart" href="#"><i style="color:blue; font-size: 1.3em" class="fa fa-heart-o"></i></a>
                                        </div> --}}
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- end single product -->
                </div>
                <!-- end products row -->
            </div>
            <!-- end products section -->
        </div>
    </div>
    <!-- end page content -->
@endsection

@section('script')
    {{-- fungsi read notif di database --}}
    <script>
        function read() {
            $.get("{{ url('read-notifikasi') }}", {}, function(data_notifications, status) {
                $("#read").html('');
                $("#baca").html(data_notifications['getNotif']);
                if (data_notifications['getNotif'] != null) {
                    for (let index = 0; index < data_notifications['getNotif'].length; index++) {
                        const notif = data_notifications['getNotif'][index];
                        $("#read").append(
                            '<a class="dropdown-item d-flex align-items-center" href="#" onclick="dibaca(' +
                            notif[
                                "id"] + ')">' + '<div class="mr-3">' + '<div class="icon-circle bg-primary">' +
                            '<i class="fas fa-file-alt text-white"></i>' +
                            '</div>' +
                            '</div>' +
                            '<div>' +
                            '<div class="font-weight-bold"> ' + notif["judul"] + '</div>' +
                            '<span class="small text-gray-500"> ' + notif["isi_pesan"] + '</span>' +
                            '</div>' +
                            '</a>'
                        );
                    }
                }

                // console.log(data_notifications);
            });
        }

        function dibaca(id) {
            console.log('clickNotif');
            $.get("{{ url('bacaNotif') }}/" + id, {}, function() {});
            // var selector = $("#clickNotif").html();
            // if (selector - 1 > 0) {
            //     $("#clickNotif").html(selector - 1);
            // } else {
            //     $("#clickNotif").remove();
            // }
        }

        function jumlahNotif() {
            $.get("{{ url('jumlah-notifikasi') }}", {}, function(jumlah_notifikasi, status) {
                let bell = $(".jmlh_notif");
                // if (document.getElementsByClassName("jmlh_notif")) {
                //     document.getElementsByClassName("jmlh_notif").remove();
                // }
                // if ($('#clickNotif')) {
                //     $("#clickNotif").remove();
                // }
                if (jumlah_notifikasi !== 0) {
                    // $('<span id="clickNotif" class="badge badge-danger badge-counter">' + jumlah_notifikasi +
                    //         '</span>')
                    //     .appendTo(
                    //         ".bara");
                    $('#clickNotif').removeClass('d-none');
                    $("#clickNotif").html(jumlah_notifikasi);
                } else {
                    $('#clickNotif').addClass('d-none');
                }


            });
        }
    </script>

    {{-- fungsi create notifikasi --}}
    {{-- <script>
        function create() {
            $.get("{{ url('create') }}", {}, function(data_notifications, status) {
                $("#exampleModalLabel").html('Create Product')
                $("#page").html('data');
                $("#exampleModal").html('show');
            });
        }
    </script> --}}

    {{-- ### Script Nampilin Notifikasi ### --}}
    <script>
        function tampilnotifikasi(data) {
            console.log('coba');
            if (Notification.premission === "granted") {
                navigator.serviceWorker.ready.then((registration) => {
                    registration.showNotification(data['judul'], {
                        body: data['isi_pesan'],
                        icon: "/logo.png",
                    });
                });
            } else if (Notification.permission !== "denied") {
                Notification.requestPermission(function(result) {
                    if (result === 'granted') {
                        navigator.serviceWorker.ready.then((registration) => {
                            registration.showNotification(data['judul'], {
                                body: data['isi_pesan'],
                                icon: "/logo.png",
                            });
                        });
                    }
                });
            }
        }

        function AjaxFunction() {
            var httpxml = new XMLHttpRequest();

            function stateck() {
                if (httpxml.readyState == 4) {
                    var response = httpxml.responseText ? JSON.parse(httpxml.responseText) : false;
                    if (response) {
                        tampilnotifikasi(response);
                        console.log(response);
                    } else {
                        return false;
                    }
                }
            }
            httpxml.onreadystatechange = stateck;
            httpxml.open("GET", "/user-getnotifpop", true);
            httpxml.send();
        }
    </script>


    {{-- <script>
    //     navigator.serviceWorker.register('sw.js');
    //     Notification.requestPermission(function(result) {
    //         if (result === 'granted') {
    //             navigator.serviceWorker.ready.then((registration) => {
    //                 registration.showNotification('{{ $notif->judul }}', {
    //                     body: '{{ $notif->isi_pesan }}',
    //                     icon: "/logo.png",
    //                 });
    //                 // console.log(registration);
    //             });
    //         }
    //         setInterval(read, 1000);
    //     });
    // </script> --}}

    {{-- pwa --}}
    {{-- <meta name="theme-color" content="#6777ef" />
    <link rel="apple-touch-icon" href="{{ asset('logo.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}"> --}}

    {{-- pwa --}}
    {{-- <script src="{{ asset('/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("/sw.js").then(function(reg) {
                console.log("Service worker has been registered for scope: " + reg.scope);
            });
        }
    </script> --}}

    {{-- untuk proses create notifikasi --}}
    {{-- <script>
        function store() {
            var live_notifications = $("#live_notifications").val();
            $.ajax({
                type: "get",
                url: "{{ url('store') }}",
                data_notifications: "live_notifications" + live_notifications,
                success: function(data_notifications) {
                    read()
                }

            });
        }
    </script> --}}
@endsection
