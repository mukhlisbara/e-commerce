@extends('layouts.app')
@section('content')
    <div class="container mt-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary align-right">Data Transaction</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0"
                                    role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                    <thead>
                                        <tr role="row">
                                            <th>Customer</th>
                                            <th>Order</th>
                                            <th>Total</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($orders as $item)
                                            <tr>
                                                <td>{{ $item->user->name }}</td>
                                                <td>
                                                    @foreach ($item->detail as $detail)
                                                        {{ "{$detail->product->nama} x{$detail->jumlah}" }} <br>
                                                    @endforeach
                                                </td>
                                                <td>{{ "Rp. {$item->total}" }}</td>
                                                <td><a href="{{ route('admin.approved', ['id_admin' => Auth::id(), 'id_order' => $item->id]) }}"
                                                        class="btn btn-success btn-sm active ok" role="button"
                                                        aria-pressed="true"><i class="fa-solid fa-check">Approved</i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        {{-- <form action="" method="post">
                                            <td>user 1</td>
                                            <td>ventela high</td>
                                            <td>1pcs</td>
                                            <td>Rp. 300.000</td>
                                            <td> <button type="submit">Approved</button></td>
                                        </form>

                                        @forelse ($orders as $order)
                                            <tr>
                                                <td>
                                                    <span class="font-weight-bold">{{ $order->customer->nama }}</span>
                                                </td>
                                                <td>
                                                    @foreach ($order->detail as $detail)
                                                        {{ $detail->product->nama }} Qty : {{ $detail->jumlah }} <br>
                                                    @endforeach
                                                </td>
                                                <td>@currency($order->total)</td>
                                                <td>
                                                    @if ($order->is_success === 0)
                                                        <a class="text-success approve"
                                                            href="{{ route('admin.approve', ['id_admin' => Auth::id(), 'id_order' => $order->id]) }}">
                                                            <i data-feather="check" width="14" height="14"></i>
                                                            <span>Approve</span>
                                                        </a>
                                                    @else
                                                        <span class="text-success font-weight-bold"><i data-feather="check"
                                                                width="14" height="14"></i> Approved</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4" class="text-center">DATA TIDAK DITEMUKAN !!</td>
                                            </tr>
                                        @endforelse --}}

                                        {{-- @foreach ($qrcodes as $item)
                                        <tr>

                                            <td>{{ QrCode::size(200)->generate($item->qrcode) }}</td>
                                            <td>{{ $item->qrcode }}</td>
                                            <td><a href="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(300)->generate($qrcode)) }}"
                                                    download="qrcode"><button type="download"
                                                        class="btn btn-primary">Download</button></a></a>
                                            </td>
                                        </tr>
                                    @endforeach --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form id="approve" method="POST" action="#">
        @csrf
        @method('POST')
    </form>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();
        });
    </script>

    {{-- sweet alert --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.26/dist/sweetalert2.all.min.js"></script>
    <script>
        var ok = document.getElementsByClassName("ok");
        for (var i = 0; i < ok.length; i++) {
            ok[i].addEventListener('click', function(e) {
                e.preventDefault();
                var tujuan = this.href;
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#28a745',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Approved Now!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = (tujuan);
                        Swal.fire({
                            title: 'Action Success!',
                            html: 'Your action has been success.',
                            icon: 'success',
                            showConfirmButton: false,
                        });
                    }
                })

            });
        }
    </script>
@endsection
