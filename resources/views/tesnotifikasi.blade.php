<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <h4> Ada Notifikasi Baru</h4>
    {{-- ### Script Nampilin Notifikasi ### --}}
    <script>
        navigator.serviceWorker.register('sw.js');
        Notification.requestPermission(function(result) {
            if (result === 'granted') {
                navigator.serviceWorker.ready.then((registration) => {
                    registration.showNotification('{{ $notif->judul }}', {
                        body: '{{ $notif->isi_pesan }}',
                        icon: "/logo.png",
                    });
                    console.log(registration);
                });
            }
        });
    </script>
</body>

</html>
