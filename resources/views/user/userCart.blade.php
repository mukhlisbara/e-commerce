@extends('layouts.app')

@section('style')
    <!-- Custom styles for this page -->
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <!-- Start Hero Section -->
    <div class="hero">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-5">
                    <div class="intro-excerpt">
                        <h1>Cart</h1>
                    </div>
                </div>
                <div class="col-lg-7">

                </div>
            </div>
        </div>
    </div>
    <!-- End Hero Section -->

    <div class="untree_co-section before-footer-section">
        <div class="container">
            <div class="row mb-5">
                <form class="col" method="post">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="product-thumbnail">
                                        <img src="{{ asset('/img/portfolio/7.jpg') }}" height="100px" width="100px"
                                            alt="Image" class="img-fluid">
                                    </td>
                                    <td class="product-name">
                                        <h2 class="h5 text-black">Ventela High Public Green Army</h2>
                                    </td>
                                    <td>Rp. 200.000</td>
                                    <td class="">
                                        <select class="quantity" data-id="825445a0977d741e32ab8351e153e126"
                                            data-productquantity="2">
                                            <option class="option" value="1">1</option>
                                            <option class="option" value="2" selected="">2</option>
                                            <option class="option" value="3">3</option>
                                            <option class="option" value="4">4</option>
                                            <option class="option" value="5">5</option>
                                            <option class="option" value="6">6</option>
                                            <option class="option" value="7">7</option>
                                            <option class="option" value="8">8</option>
                                            <option class="option" value="9">9</option>
                                        </select>
                                    </td>
                                    <td>Rp. 200.000</td>
                                    <td><a href="#" class="btn btn-danger btn-sm active  " role="button"
                                            aria-pressed="true"><i class="fa-solid fa-trash"></i></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="product-thumbnail">
                                        <img src="{{ asset('/img/portfolio/11.jpg') }}" height="100px" width="100px"
                                            alt="Image" class="img-fluid">
                                    </td>
                                    <td class="product-name">
                                        <h2 class="h5 text-black">Ventela High Public Black</h2>
                                    </td>
                                    <td>Rp. 200.000</td>
                                    {{-- <td>
                                        <div class="input-group mb-3 d-flex align-items-center quantity-container"
                                            style="max-width: 120px;">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-outline-black decrease"
                                                    type="button">&minus;</button>
                                            </div>
                                            <input type="text" class="form-control text-center quantity-amount"
                                                value="1" placeholder="" aria-label="Example text with button addon"
                                                aria-describedby="button-addon1">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-black increase"
                                                    type="button">&plus;</button>
                                            </div>
                                        </div>
                                    </td> --}}
                                    <td class="">
                                        <select class="quantity" data-id="825445a0977d741e32ab8351e153e126"
                                            data-productquantity="2">
                                            <option class="option" value="1">1</option>
                                            <option class="option" value="2" selected="">2</option>
                                            <option class="option" value="3">3</option>
                                            <option class="option" value="4">4</option>
                                            <option class="option" value="5">5</option>
                                            <option class="option" value="6">6</option>
                                            <option class="option" value="7">7</option>
                                            <option class="option" value="8">8</option>
                                            <option class="option" value="9">9</option>
                                        </select>
                                    </td>
                                    <td>Rp. 200.000</td>
                                    <td> <a href="#" class="btn btn-danger btn-sm active  " role="button"
                                            aria-pressed="true"><i class="fa-solid fa-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row mb-5">
                        <div class="col-md-6 mb-3 mb-md-0">
                            <button class="btn btn-primary btn-sm btn-block" onclick="window.location='user'">Update
                                Cart</button>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="text-black h4" for="coupon">Coupon</label>
                            <p>Enter your coupon code if you have one.</p>
                        </div>
                        <div class="col-md-8 mb-3 mb-md-0">
                            <input type="text" class="form-control py-3" id="coupon" placeholder="Coupon Code">
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-success">Apply Coupon</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 pl-5">
                    <div class="row justify-content-end">
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-12 text-right border-bottom mb-5">
                                    <h3 class="text-black h4 text-uppercase">Cart Totals</h3>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <span class="text-black">Subtotal</span>
                                </div>
                                <div class="col-md-6 text-right">
                                    <strong class="text-black">Rp. 400.000</strong>
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-md-6">
                                    <span class="text-black">Total</span>
                                </div>
                                <div class="col-md-6 text-right">
                                    <strong class="text-black">Rp. 400.000</strong>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-success btn-lg py-3 btn-block"
                                        onclick="window.location='userCheckout'">Proceed To Checkout</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/tiny-slider.js"></script>
    <script src="js/custom.js"></script>
@endsection
