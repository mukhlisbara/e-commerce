@extends('layouts.app')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Area Spline</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <link href="../../assets/styles.css" rel="stylesheet" />

    <style>
        #chart_area {
            max-width: 650px;
            margin: 35px auto;
        }

        #chart_column {
            max-width: 650px;
            margin: 35px auto;
        }
    </style>

    <script>
        window.Promise ||
            document.write(
                '<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"><\/script>'
            )
        window.Promise ||
            document.write(
                '<script src="https://cdn.jsdelivr.net/npm/eligrey-classlist-js-polyfill@1.2.20171210/classList.min.js"><\/script>'
            )
        window.Promise ||
            document.write(
                '<script src="https://cdn.jsdelivr.net/npm/findindex_polyfill_mdn"><\/script>'
            )
    </script>


    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>


    <script>
        // Replace Math.random() with a pseudo-random number generator to get reproducible results in e2e tests
        // Based on https://gist.github.com/blixt/f17b47c62508be59987b
        var _seed = 42;
        Math.random = function() {
            _seed = _seed * 16807 % 2147483647;
            return (_seed - 1) / 2147483646;
        };
    </script>


</head>

@section('content')

    <body>
        <div class="row">
            {{-- chart column --}}
            <div class="col-4">

                <div id="chart_column"></div>
                <script>
                    var options = {
                        series: [{
                            name: 'Net Profit',
                            data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
                        }, {
                            name: 'Revenue',
                            data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
                        }, {
                            name: 'Free Cash Flow',
                            data: [35, 41, 36, 26, 45, 48, 52, 53, 41]
                        }],
                        chart: {
                            type: 'bar',
                            height: 350
                        },
                        plotOptions: {
                            bar: {
                                horizontal: false,
                                columnWidth: '55%',
                                endingShape: 'rounded'
                            },
                        },
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            show: true,
                            width: 2,
                            colors: ['transparent']
                        },
                        xaxis: {
                            categories: ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],
                        },
                        yaxis: {
                            title: {
                                text: '$ (thousands)'
                            }
                        },
                        fill: {
                            opacity: 1
                        },
                        tooltip: {
                            y: {
                                formatter: function(val) {
                                    return "$ " + val + " thousands"
                                }
                            }
                        }
                    };

                    var chart_column = new ApexCharts(document.querySelector("#chart_column"), options);
                    chart_column.render();
                </script>
            </div>
            {{-- chart area --}}
            <div class="col-4">

                <div id="chart_area"></div>

                <script>
                    var options = {
                        series: [{
                            name: 'series1',
                            data: [31, 40, 28, 51, 42, 109, 100]
                        }, {
                            name: 'series2',
                            data: [11, 32, 45, 32, 34, 52, 41]
                        }],
                        chart: {
                            height: 350,
                            type: 'area'
                        },
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            curve: 'smooth'
                        },
                        xaxis: {
                            type: 'datetime',
                            categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z",
                                "2018-09-19T03:30:00.000Z", "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z",
                                "2018-09-19T06:30:00.000Z"
                            ]
                        },
                        tooltip: {
                            x: {
                                format: 'dd/MM/yy HH:mm'
                            },
                        },
                    };

                    var chart_area = new ApexCharts(document.querySelector("#chart_area"), options);
                    chart_area.render();
                </script>
            </div>
            {{-- chart pie --}}
            <div class="col-4">

                <div id="chart_simple"></div>

                <script>
                    var options = {
                        series: [44, 55, 13, 43, 22],
                        chart: {
                            width: 380,
                            type: 'pie',
                        },
                        labels: ['Team A', 'Team B', 'Team C', 'Team D', 'Team E'],
                        responsive: [{
                            breakpoint: 480,
                            options: {
                                chart: {
                                    width: 200
                                },
                                legend: {
                                    position: 'bottom'
                                }
                            }
                        }]
                    };

                    var chart_simple = new ApexCharts(document.querySelector("#chart_simple"), options);
                    chart_simple.render();
                </script>
            </div>
            {{-- chart bar --}}
            <div class="col-4">
                <div id="chart_bar"></div>

                <script>
                    var options = {
                        series: [{
                            data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380]
                        }],
                        chart: {
                            type: 'bar',
                            height: 350
                        },
                        plotOptions: {
                            bar: {
                                borderRadius: 4,
                                horizontal: true,
                            }
                        },
                        dataLabels: {
                            enabled: false
                        },
                        xaxis: {
                            categories: ['South Korea', 'Canada', 'United Kingdom', 'Netherlands', 'Italy', 'France', 'Japan',
                                'United States', 'China', 'Germany'
                            ],
                        }
                    };

                    var chart_bar = new ApexCharts(document.querySelector("#chart_bar"), options);
                    chart_bar.render();
                </script>
            </div>
            {{-- chart line --}}
            <div class="col-4">
                <div id="chart_line"></div>
                <script>
                    var options = {
                        series: [{
                            name: "Desktops",
                            data: [10, 41, 35, 51, 49, 62, 69, 91, 148]
                        }],
                        chart: {
                            height: 350,
                            type: 'line',
                            zoom: {
                                enabled: false
                            }
                        },
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            curve: 'straight'
                        },
                        title: {
                            text: 'Product Trends by Month',
                            align: 'left'
                        },
                        grid: {
                            row: {
                                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                                opacity: 0.5
                            },
                        },
                        xaxis: {
                            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
                        }
                    };

                    var chart_line = new ApexCharts(document.querySelector("#chart_line"), options);
                    chart_line.render();
                </script>


                <script>
                    var options = {
                        series: [44, 55, 13, 33],
                        chart: {
                            width: 380,
                            type: 'donut',
                        },
                        dataLabels: {
                            enabled: false
                        },
                        responsive: [{
                            breakpoint: 480,
                            options: {
                                chart: {
                                    width: 200
                                },
                                legend: {
                                    show: false
                                }
                            }
                        }],
                        legend: {
                            position: 'right',
                            offsetY: 0,
                            height: 230,
                        }
                    };

                    var chart = new ApexCharts(document.querySelector("#chart"), options);
                    chart.render();


                    function appendData() {
                        var arr = chart.w.globals.series.slice()
                        arr.push(Math.floor(Math.random() * (100 - 1 + 1)) + 1)
                        return arr;
                    }

                    function removeData() {
                        var arr = chart.w.globals.series.slice()
                        arr.pop()
                        return arr;
                    }

                    function randomize() {
                        return chart.w.globals.series.map(function() {
                            return Math.floor(Math.random() * (100 - 1 + 1)) + 1
                        })
                    }

                    function reset() {
                        return options.series
                    }

                    document.querySelector("#randomize").addEventListener("click", function() {
                        chart.updateSeries(randomize())
                    })

                    document.querySelector("#add").addEventListener("click", function() {
                        chart.updateSeries(appendData())
                    })

                    document.querySelector("#remove").addEventListener("click", function() {
                        chart.updateSeries(removeData())
                    })

                    document.querySelector("#reset").addEventListener("click", function() {
                        chart.updateSeries(reset())
                    })
                </script>
            </div>
            {{-- chart donut --}}
            <div class="col-4">
                <div id="chart_donut"></div>
                <script>
                    var options = {
                        series: [44, 55, 41, 17, 15],
                        chart: {
                            type: 'donut',
                        },
                        responsive: [{
                            breakpoint: 480,
                            options: {
                                chart: {
                                    width: 200
                                },
                                legend: {
                                    position: 'bottom'
                                }
                            }
                        }]
                    };

                    var chart_donut = new ApexCharts(document.querySelector("#chart_donut"), options);
                    chart_donut.render();
                </script>
            </div>

        </div>




    </body>
@endsection

</html>
