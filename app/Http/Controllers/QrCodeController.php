<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserNotification;
use App\Models\Qrcode;
use Illuminate\Support\Facades\Auth;

class QrCodeController extends Controller
{
    public function index()
    {
        // Gate::authorize('admin');qr
        return view('qrcode')->with(['data_notifications' => $this->alertnotif]);
    }

    public function create()
    {
        // Gate::authorize('admin');
        // return view('qrcode')->with(
        //     ['qrcode' => 'Halo', 'data_notifications' => $this->alertnotif]
        // );
        $qrcodes = Qrcode::latest()->get();


        return view('qrcode')->with([
            'qrcode' => $qrcodes->count() != 0 ? $qrcodes[0]->qrcode : 'Wow',
            'qrcodes' => $qrcodes->count() != 0 ? $qrcodes : NULL,

            // 'qrcode' => $qrcodes[0]->qrcode,
            // 'qrcodes' => $qrcodes,
            'data_qrcodes' => $this->alertnotif,
            'data_notifications' => $this->alertnotif
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'qrcode' => 'required',
        ]);

        $qrcode = new Qrcode($request->except('_token'));
        $qrcode->save();

        return redirect()->route('admin.create-qrcode')->with(
            ['create-qrcode' => $request->qrcode, 'data_qrcodes' => $this->alertnotif]
        );
    }

    public function __construct()
    {
        $this->alertnotif = UserNotification::latest()->take(5)->get();
    }

    protected $alertnotif;
}
