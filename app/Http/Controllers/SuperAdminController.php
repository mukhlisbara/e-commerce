<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserNotification;
use Illuminate\Http\Request;


class SuperAdminController extends Controller
{
    public function index()
    {
        $admins = User::where('id_role', 2)->get();

        return view('superadmin.addAdmin', [
            'data_admins' => $admins,
            'data_notifications' => $this->alertnotif
        ]);
    }

    public function indexUser()
    {
        $users = User::where('id_role', 3)->get();

        return view('superadmin.listUser', [
            'data_users' => $users,
            'data_notifications' => $this->alertnotif
        ]);
    }

    public function delete($id)
    {
        $admin = User::find($id);

        $admin->delete();

        return back();
    }

    public function __construct()
    {
        $this->alertnotif = UserNotification::latest()->take(5)->get();
    }

    protected $alertnotif;
}
