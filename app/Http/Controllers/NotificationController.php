<?php

namespace App\Http\Controllers;

use App\Models\UserNotification;
use App\Models\AdminNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function index()
    {
        $notif = UserNotification::where('id_user', NULL)->orWhere('id_user', Auth::id())->get();
        return view('userHome')->with(['notif' => $notif[$notif->count() - 1]]);
    }

    public function create()
    {
        return view('home');
    }

    public function read()
    {
        // $data = Notification::whereNotNull('id_customer')->whereNull('id_admin')->where('dibaca', 0)->get()->count();
        // if ($data) {
        //     return response()->json($data);
        // } else {
        //     return false;
        // }
        if (Auth::user()->id_role === 3) {
            $data['count'] = UserNotification::where('dibaca', 0)->orwhere('id_user', NULL)->orWhere('id_user', Auth::id())->latest()->take(5)->count();
            $data['getNotif'] = UserNotification::where('dibaca', 0)->where(function ($query) {
                $query->where('id_user', NULL);
                $query->orwhere('id_user', Auth::id());
            })->latest()->take(5)->get();
            return response()->json($data);
        };

        if (Auth::user()->id_role === 2) {
            $data['count'] = AdminNotification::where('dibaca', 0)->latest()->take(5)->count();
            $data['getNotif'] = AdminNotification::where('dibaca', 0)->latest()->take(5)->get();
            return response()->json($data);
        };
    }


    public function jumlahNotif()
    {
        if (Auth::user()->id_role === 2) {
            $data = AdminNotification::where('dibaca', 0)->latest()->get()->count();
            return response()->json($data);
        };

        if (Auth::user()->id_role === 3) {
            // $data = UserNotification::where('dibaca', 0)->where('id_user', NULL)->orWhere('id_user', Auth::id())->latest()->get()->count();
            $data = UserNotification::where('dibaca', 0)->where(function ($query) {
                $query->where('id_user', NULL);
                $query->orwhere('id_user', Auth::id());
            })->latest()->get()->count();
            return response()->json($data);
        };
    }

    public function bacaNotif($id)
    {
        if (Auth::user()->id_role === 2) {
            $baca = AdminNotification::find($id);
            $baca->dibaca = true;
            $baca->save();
        };
        if (Auth::user()->id_role === 3) {

            $baca = UserNotification::find($id);
            $baca->dibaca = true;
            $baca->save();
        };
    }

    public function getNotifPop()
    {
        if (Auth::user()->id_role === 2) {
            $notif = AdminNotification::where('popup', 0)->where(function ($query) {
                $query->where('id_admin', NULL);
                $query->orwhere('id_admin', Auth::id());
            })->oldest()->first();
            if ($notif) {
                $notif->popup = 1;
                $notif->save();
                // dd(response()->json($notif));
                return response()->json($notif);
            }
            return false;
        };

        if (Auth::user()->id_role === 3) {
            // $notif = UserNotification::where('id_user', NULL)->orWhere('id_user', Auth::id())->where('popup', 0)->oldest()->first();
            $notif = UserNotification::where('popup', 0)->where(function ($query) {
                $query->where('id_user', NULL);
                $query->orwhere('id_user', Auth::id());
            })->oldest()->first();
            if ($notif) {
                $notif->popup = 1;
                $notif->save();
                // dd(response()->json($notif));
                return response()->json($notif);
            }
            return false;
        };
    }


    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'judul' => 'required',
            'isi_pesan' => 'required',
        ]);

        $notif = new UserNotification($request->except('_token'));
        // $notif->id_user = Auth::user()->id;
        $notif->save();

        return redirect()->route('admin.home');

        $data['live_notification'] = $request->live_notification;
        UserNotification::insert($data);
    }
}
