<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Models\AdminNotification;
use App\Models\UserNotification;
use App\Models\Order;
use App\Models\DetailOrder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct()
    {
        $this->alertnotif = UserNotification::where('id_user', NULL)->orWhere('id_user', Auth::id())->latest()->take(5)->get();
        // $thisCount = $this->count();
    }
    protected $alertnotif;

    public function cart()
    {
        $carts = User::where('id_role', 3)->get();
        $notif = UserNotification::where('id_user', NULL)->orWhere('id_user', Auth::id())->get();
        return view('user.userCart', [
            'data_carts' => $carts,
            'data_notifications' => $this->alertnotif,
            'notif' => $notif->count() > 0 ? $notif[$notif->count() - 1] : null,
        ]);
    }

    public function checkout()
    {
        $checkout = User::where('id_role', 3)->get();
        $notif = UserNotification::where('id_user', NULL)->orWhere('id_user', Auth::id())->get();

        return view('user.userCheckout', [
            'data_checkout' => $checkout,
            'data_notifications' => $this->alertnotif,
            'notif' => $notif->count() > 0 ? $notif[$notif->count() - 1] : null,
        ]);
    }

    public function orderNow($id)
    {
        $order = new Order();
        $order->id_user = $id;
        $order->save();

        for ($i = 1; $i <= 2; $i++) {
            $detail_order = new DetailOrder();
            $detail_order->id_product = $i;
            $detail_order->id_order = $order->id;
            $detail_order->jumlah = 1;
            $detail_order->save();
            $order->total += $detail_order->product->harga;
        }

        $order->save();
        $notif = new AdminNotification();
        $notif->judul = "Ada Pembelian dari " . Auth::user()->name;
        $notif->isi_pesan = Auth::user()->name . " sudah melakukan checkout pembelian, harap segera acc !!";
        // $notif->id_admin = $id;
        // $notif->link = route('admin.adminTransaction');
        $notif->timestamps = now();
        $notif->save();
        return redirect()->route('user.home');
    }

    public function notification()
    {
        $notif = UserNotification::where('id_user', NULL)->orWhere('id_user', Auth::id())->get();
        return view('userHome')->with([
            // 'notif' => $notif[$notif->count() - 1],
            'data_notifications' => $this->alertnotif
        ]);
    }
}
