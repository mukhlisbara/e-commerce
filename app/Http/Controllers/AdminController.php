<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\AdminNotification;
use App\Models\Order;
use App\Models\UserNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function countgender($users)
    {
        return [
            'Laki_laki' => $users->where('gender', 1)->count(),
            'Perempuan' => $users->where('gender', 2)->count(),
        ];
    }

    public function countaddress($users)
    {
        return [
            'surabaya' => $users->where('address', 'Surabaya')->count(),
            'malang' => $users->where('address', 'Malang')->count(),
        ];
    }

    public function indexAdmin()
    {
        $notif = UserNotification::where('id_user', NULL)->orWhere('id_user', Auth::id())->get();

        // $notif = AdminNotification::where('id_admin', NULL)->orWhere('id_admin', Auth::id())->get();
        $users = $this->countgender(User::where('id_role', '=', 3)->get());
        $address = $this->countaddress(User::where('id_role', '=', 3)->get());
        // dd($notif);
        return view('adminHome')->with([
            'Laki_laki' => $users['Laki_laki'],
            'Perempuan' => $users['Perempuan'],
            'surabaya' => $address['surabaya'],
            'malang' => $address['malang'],
            // 'notif' => $this->getnotif(),
            'data_notifications' => $this->alertnotif,
            // 'notif' => $notif ? $notif[$notif->count() - 1] : null,
            // 'notif' => $notif[$notif->count() - 1],
        ]);
    }

    public function indexUser()
    {
        $users = User::where('id_role', 3)->get();
        $notif = UserNotification::where('id_user', NULL)->orWhere('id_user', Auth::id())->get();

        return view('superadmin.listUser', [
            'data_users' => $users,
            'data_notifications' => $this->alertnotif,
            // 'notif' => $notif[$notif->count() - 1],
        ]);
    }


    public function transaction()
    {
        $orders = Order::all();
        $notif = UserNotification::where('id_user', NULL)->orWhere('id_user', Auth::id())->get();

        // dd($orders[0]->detail);
        return view('admin.adminTransaction')->with([
            'data_transaction' => $orders,
            'orders' => $orders,
            'data_notifications' => $this->alertnotif,
            // 'notif' => $notif[$notif->count() - 1],

        ]);
    }

    public function notification()
    {
        // $notif = AdminNotification::all();
        $notif = AdminNotification::where('id_admin', NULL)->orWhere('id_admin', Auth::id())->get();

        return view('adminHome')->with([
            // 'notif' => $notif[$notif->count() - 1],
            'data_notifications' => $this->alertnotif,
        ]);
    }

    public function delete($id)
    {
        $admin = User::find($id);

        $admin->delete();

        return back();
    }


    public function approved($id_admin, $id_order)
    {
        $order = Order::find($id_order);
        // dd($order);
        $order->is_success = 1;
        $order->save();

        $notif = new UserNotification();
        $notif->judul = "Order telah di proses !!";
        $notif->isi_pesan = "Ordermu telah disetujui oleh Admin " . Auth::user()->nama . ", Terima kasih telah belanja !!";
        // $notif->id_admin = $id_admin;
        $notif->id_user = $order->id_user;
        $notif->timestamps = now();
        $notif->save();

        return redirect()->route('admin.admin-transaction');
    }

    public function __construct()
    {
        $this->alertnotif = UserNotification::latest()->take(5)->get();
    }

    protected $alertnotif;
}
