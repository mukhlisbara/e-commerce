<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $fillable = [
        'judul',
        'isi_pesan',
        'id_user',
        'dibaca',
        'popup',
    ];
    use HasFactory;
}
