<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => "Mas Superadmin",
                'gender' => 1,
                'id_role' => 1,
                'email' => 'superadmin@domain.com',
                'address' => 'Surabaya',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mas Admin",
                'store_name' => "Berkah Makmur",
                'id_role' => 2,
                'email' => 'admin@domain.com',
                'gender' => 1,
                'address' => 'Surabaya',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mbak Admin",
                'store_name' => "Berkah Abadi",
                'id_role' => 2,
                'email' => 'adminp@domain.com',
                'gender' => 2,
                'address' => 'Surabaya',
                'phone' => '+6288888888899',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mas User",
                'id_role' => 3,
                'email' => 'user@domain.com',
                'gender' => 1,
                'address' => 'Surabaya',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mbak User",
                'id_role' => 3,
                'email' => 'userp@domain.com',
                'gender' => 2,
                'address' => 'Surabaya',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mas User1",
                'id_role' => 3,
                'email' => 'user1@domain.com',
                'gender' => 1,
                'address' => 'Surabaya',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mbak User1",
                'id_role' => 3,
                'email' => 'userp1@domain.com',
                'gender' => 2,
                'address' => 'Malang',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mas User2",
                'id_role' => 3,
                'email' => 'user2@domain.com',
                'gender' => 1,
                'address' => 'Surabaya',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mbak User2",
                'id_role' => 3,
                'email' => 'userp2@domain.com',
                'gender' => 2,
                'address' => 'Malang',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mas User3",
                'id_role' => 3,
                'email' => 'user3@domain.com',
                'gender' => 1,
                'address' => 'Surabaya',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mbak User3",
                'id_role' => 3,
                'email' => 'userp3@domain.com',
                'gender' => 2,
                'address' => 'Malang',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mas User4",
                'id_role' => 3,
                'email' => 'user4@domain.com',
                'gender' => 1,
                'address' => 'Surabaya',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mbak User4",
                'id_role' => 3,
                'email' => 'userp4@domain.com',
                'gender' => 2,
                'address' => 'Malang',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mas User5",
                'id_role' => 3,
                'email' => 'user5@domain.com',
                'gender' => 1,
                'address' => 'Surabaya',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mbak User5",
                'id_role' => 3,
                'email' => 'userp5@domain.com',
                'gender' => 2,
                'address' => 'Malang',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mas User6",
                'id_role' => 3,
                'email' => 'user6@domain.com',
                'gender' => 1,
                'address' => 'Surabaya',
                'phone' => '+628123656789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mbak User6",
                'id_role' => 3,
                'email' => 'userp6@domain.com',
                'gender' => 2,
                'address' => 'Malang',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mas User7",
                'id_role' => 3,
                'email' => 'user7@domain.com',
                'gender' => 1,
                'address' => 'Surabaya',
                'phone' => '+628123756789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mbak User7",
                'id_role' => 3,
                'email' => 'userp7@domain.com',
                'gender' => 2,
                'address' => 'Malang',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mas User8",
                'id_role' => 3,
                'email' => 'user8@domain.com',
                'gender' => 1,
                'address' => 'Surabaya',
                'phone' => '+628123856789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mbak User8",
                'id_role' => 3,
                'email' => 'userp8@domain.com',
                'gender' => 2,
                'address' => 'Malang',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mas User9",
                'id_role' => 3,
                'email' => 'user9@domain.com',
                'gender' => 1,
                'address' => 'Surabaya',
                'phone' => '+628123956789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mbak User9",
                'id_role' => 3,
                'email' => 'userp9@domain.com',
                'gender' => 2,
                'address' => 'Malang',
                'phone' => '+628123456789',
                'password' => bcrypt('123123123'),
            ],
            [
                'name' => "Mas User10",
                'id_role' => 3,
                'email' => 'user10@domain.com',
                'gender' => 1,
                'address' => 'Surabaya',
                'phone' => '+6281231056789',
                'password' => bcrypt('123123123'),
            ],

        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
