<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                // 'gambar' => '',
                'nama' => 'Ventela High Public Green Army',
                'deskripsi' => 'Lorem ipsum ABC',
                'harga' => 200000,
            ],
            [
                // 'gambar' => '',
                'nama' => 'Ventela High Public Black',
                'deskripsi' => 'Lorem ipsum ABC',
                'harga' => 200000,
            ],
        ];

        foreach ($datas as $key => $value) {
            DB::table('products')->insert($value);
        }
    }
}
