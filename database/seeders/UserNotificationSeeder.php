<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserNotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'id_user' => '1',
                'judul' => 'Pesan Baru dari Admin!',
                'isi_pesan' => 'Jangan Sampai Lupakan Promo Hari Ini!!',
            ]
        ];

        foreach ($datas as $key => $value) {
            DB::table('user_notifications')->insert($value);
        }
    }
}
