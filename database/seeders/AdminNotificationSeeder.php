<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminNotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'id_admin' => '1',
                'judul' => 'Ada Pembelian dari User',
                'isi_pesan' => 'User sudah melakukan checkout pembelian, harap segera acc !!',
            ]
        ];

        foreach ($datas as $key => $value) {
            DB::table('admin_notifications')->insert($value);
        }
    }
}
